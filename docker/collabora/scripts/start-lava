#!/bin/sh

set -e

if [ ! -f docker/collabora/Dockerfile.build ]; then
    echo 'You need to run this script from the root of the repository.'
    exit 1
fi

COMPOSE="docker-compose -f docker/collabora/docker-compose.yaml -f docker/collabora/docker-compose.ci.yaml"

# docker-compose expects to find this file, even for the 'down' action, make sure we
# have something there.
test -f docker/collabora/lava-env-dispatcher || \
    cp docker/collabora/lava-env-dispatcher.in \
       docker/collabora/lava-env-dispatcher

# Make sure our services are down and empty
$COMPOSE down
docker volume rm -f collabora_lavadbdata || true

# Start the LAVA container and wait for it to reply something that makes sense, meaning
# it actually started gunicorn.
$COMPOSE up --detach lava

# Keep track of LAVA's startup
docker logs -f collabora-lava-1 &

# We use docker to check if LAVA has finished starting since we can
# plug directly into the collabora_default network, and avoid any
# issues with docker-in-docker not exposing the porti on localhost.
docker pull curlimages/curl
CURL="docker run --rm --network collabora_default curlimages/curl"

echo 'Waiting on LAVA to start...'
TIMEOUT=30
while ! $CURL -s http://lava:8000/ 2>&1 > /dev/null; do
    if [ $TIMEOUT = 0 ]; then
        docker ps
        exit 1
    fi
    TIMEOUT=$(($TIMEOUT - 1))

    sleep 1
done

DOCKER="docker exec collabora-lava-1"

# Pre-add user, device type, device, and device dictionary
$DOCKER lava-server manage users add lava --passwd lava
$DOCKER lava-server manage authorize_superuser --username lava

# Add the worker and get the token, so we can then start the dispatcher service
TOKEN=$($DOCKER lava-server manage workers add ci.worker | cut -d ' ' -f 2)
sed s/@@TOKEN@@/$TOKEN/ docker/collabora/lava-env-dispatcher.in > docker/collabora/lava-env-dispatcher

$COMPOSE up --no-recreate --detach lava-dispatcher

# Add the device
$DOCKER lava-server manage devices add --device-type qemu --worker ci.worker qtest
