#!/bin/sh

set -e

DATABASE=`python3 -c 'import os; from urllib.parse import urlparse; url = urlparse(os.environ["DATABASE_URL"]); print("{}:{}".format(url.hostname, url.port), end="")'`

/usr/bin/wait-for-it -t 30 $DATABASE

# Make sure configuration directories are available and have the appropriate
# ownership and permissions.
mkdir -p /etc/lava-server/settings.d
cp /logging_env.yaml /etc/lava-server/settings.d

mkdir -p /etc/lava-server/dispatcher.d
chown -R lavaserver: /etc/lava-server/dispatcher.d

mkdir -p /etc/lava-server/dispatcher-config/devices
mkdir -p /etc/lava-server/dispatcher-config/device-types
mkdir -p /etc/lava-server/dispatcher-config/health-checks
chown -R lavaserver: /etc/lava-server/dispatcher-config

# These are based on the commands called by the package's
# /usr/share/lava-server/postinst.py.
lava-server manage migrate --noinput --fake-initial
lava-server manage makemigrations
lava-server manage migrate --noinput

lava-server manage drop_materialized_views
lava-server manage refresh_queries --all

# Make all device types available by default.
lava-server manage device-types add '*'

# These variables are based on the systemd services shipped by the package.
# Most of them are optional, see the sample blank-env file.
if [ -z "$LOGLEVEL" ]; then
    LOGLEVEL=DEBUG
fi

/usr/bin/lava-server manage lava-publisher --host '*' --port 8001 &

while :; do
    /usr/bin/lava-server manage lava-scheduler $EVENT_URL $IPV6
done &

if [ -z "$WORKERS" ]; then
    WORKERS=4
fi

if [ -z "$WORKER_CLASS" ]; then
    WORKER_CLASS=eventlet
fi

if [ -z "$BIND" ]; then
    BIND="--bind 0.0.0.0:8000"
fi

exec /usr/bin/gunicorn3 lava_server.wsgi -u lavaserver -g lavaserver --worker-class $WORKER_CLASS --workers $WORKERS $BIND $RELOAD $TIMEOUT
