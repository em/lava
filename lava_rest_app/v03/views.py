# -*- coding: utf-8 -*-
# Copyright (C) 2019 Linaro Limited
#
# Author: Stevan Radaković <stevan.radakovic@linaro.org>
#
# This file is part of LAVA.
#
# LAVA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation
#
# LAVA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with LAVA.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import annotations

from rest_framework.pagination import CursorPagination

from lava_rest_app.v02.views import TestJobViewSet as TestJobViewSetv02


class TestJobPaginator(CursorPagination):
    page_size_query_param = "limit"
    max_page_size = 100_000


class TestJobViewSet(TestJobViewSetv02):
    pagination_class = TestJobPaginator
    ordering = "id"

    validate = None
    validate_testdef = None
    resubmit = None
    cancel = None
    metadata = None
